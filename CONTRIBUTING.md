# Contributing

## How to contribute

src_prepare-overlay is very open to contribution and encourages it.
There are several ways to contribute to this overlay:

#### Submitting an issue

Those that do not have good knowledge in writing ebuilds can
[submit an issue](https://gitlab.com/src_prepare/src_prepare-overlay/-/issues)
so we can create the ebuild and maintain it for the community.
Before submitting an issue, you will have to read the [submitting an issue guidelines](#submitting-an-issue-1).

#### Submitting Merge Requests

Those that want to help us easily maintain an ebuild without making us spend our time creating the ebuild
can create the ebuild and submit a [merge request](https://gitlab.com/src_prepare/src_prepare-overlay/-/merge_requests)
so we can accept and add the ebuild to our overlay and maintain it for you.
Before submitting a merge request, you will have to read the [submitting merge requests guidelines](#submitting-merge-requests-1).

## Contributing guidelines

Before contributing, you will have to read the contributing guidelines as we will **not** accept your request
if you do not respect our community guidelines.

**Before submitting an issue or submitting a Merge Request,
first check if the package you are providing is not in [TODO.md](/TODO.md).**

#### Submitting an issue

When submitting an issue you will have to provide:

- `category/package` (e.g. `www-client/icecat`)
- the description of the package
- the home page of the package 
- OPTIONAL: the direct download link of the package 
- the license of the package (e.g. GPLv2)

by submitting the skeleton of the ebuild in quotes:

```
NAME=""

DESCRIPTION=""     
HOMEPAGE=""     
SRC_URI=""     
LICENSE=""     
```

After this, you can safely submit the issue, and we will take care of it.

#### Submitting Merge Requests

When committing and/or submitting merge requests, you will have to respect our
Committing & Submitting Merge Requests System (CnPMRS)
so we can easily identify the changes:

- Provide us the Manifest file;
- Follow the committing & submitting merge request layout (below).

Each commit will have to be based on this layout:

`category/package: [COMMIT_MESSAGE]`

For example, if the package is called **mail-client/freelook-bin**,
and you have added this package, the commit or merge request should look like this:

`mail-client/freelook-bin: add freelook-bin-[PACKAGE_VERSION]` # e.g. 1.0.0

For example, if the package is called **mail-client/freelook-bin**,
and you have fixed an error, the commit or merge request should look like this:

`mail-client/freelook-bin: fix QA - EROOT missing slash`

**This system also applies on your metadata.xml and Manifest.**