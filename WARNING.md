# WARNING!!!

Before you continue


## Read the TODO


## Known bugs

You still can file issues to let us and others know when stuff breaks

- kernel pkgs may install a different version, check the version you got after installation
- waterfox is broken, like 80% of the time, we know...


## Outdated

That's what we have euscan-ng for,
if a pkg is not updated then it's possible that the new pkg is broken


## Merge issues

Don't file bugs for those pkgs here, ask upstream for a fix

- palemoon is a resource hog during build
